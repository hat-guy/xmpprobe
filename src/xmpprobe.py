#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
XMPProbe monitors the availability of XMPP servers.

:note: There are a lot of non-constant class attributes throughout the classes of this module that aren't actually needed at all. They get overshadowed by instance attributes.

.. moduleauthor:: Damiano Boppart <hat.guy.repo@gmail.com>

Copyright 2014 Damiano Boppart

This file is part of XMPProbe.

XMPProbe is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

XMPProbe is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with XMPProbe. If not, see <http://www.gnu.org/licenses/>.
'''

# * List of interesting events to handle: session_start, message, disconnected, failed_auth, got_online, got_offline. See also: https://github.com/fritzy/SleekXMPP/wiki/Event-Index
# * Adding buddies? See: https://github.com/fritzy/SleekXMPP/wiki/Roster-Management

import sys
import platform
import logging
import argparse
import json
import pprint
import io
import csv
import random
import datetime
import pytz
import zlib
#import string
import dateutil.parser
import jsonschema
import sleekxmpp
import math
import time


class Ret(object):
	'''
	Return codes of XMPProbe.
	
	This class is used as an enum.
	'''
	
	FINE = 0  #: Success
	PYTHON_VERSION_FAIL = 1  #: Wrong Python version
	CONFIG_FILE_NOT_READABLE = 2  #: The configuration file can not be read
	CONFIG_FILE_JSON_ERROR = 3  #: The configuration file contains a JSON syntax error
	CONFIG_FILE_ERROR = 4  #: A value in the configuration file is invalid
	ARGUMENT_ERROR = 5  #: A command line argument has an invalid value


#class AccidentallyTheCode(Exception):
	#'''This exception should be raised whenever a point is reached where functionality is not implemented in a way that it makes sense to continue at all.'''
	#def __init__(self, value):
		#self.value = value
	
	#def __str__(self):
		#return repr(self.value)


class SpeedSpeedDating(object):
	'''
	Derive a solution for the *speed speed dating problem* with directed edges.
	
	After initialization an instance produces a list of partners to date in the next time slot (all partners in one matching). XMPProbe takes this list to send *ask* messages to (see :class:`Message`).
	
	The current implementation is memory inefficient: it generates all matchings completely at instance creation, and filters for the edges pertaining to the designated agent when a list of partners is requested. This allows for efficient debugging, since the entire solution to the SSD problem is represented by the internal state.
	
	:note: The internal representation of matchings :attr:`_ms` is not aware that graph edges are directed. From the set of all edges in the graph: :math:`\\{(a,b), (b,a)\\} \\forall a,b \\in V` with :math:`a \\neq b` one is arbitrarily picked for constructing all the matchings. This is consistent with constructing the solution for an undirected graph. When the list of partners for one round is derived in :meth:`_targets` the value of :attr:`_reverse` is considered to interpret an (undirected) edge :math:`(a,b)` part of a matching in :attr:`_ms` as either the directed edge :math:`(a,b)` or the directed edge :math:`(b,a)`.
	
	Usage example: ::
	
		>>> s = SpeedSpeedDating('Eve', ['Alice', 'Bob', 'Mallory', 'Eve'])
		
		>>> s.length()
		8
		>>> s.set_round(4)
		
		>>> list(s.next_())
		['Alice']
		>>> list(s.next_())
		['Mallory']
		>>> list(s.next_())
		['Bob']
		>>> list(s.next_())
		[]
		>>> list(s.next_())
		[]
		>>> list(s.next_())
		[]
		>>> list(s.next_())
		[]
		>>> list(s.next_())
		[]
		>>> list(s.next_())
		['Alice']
	
	Setup is done with the parameters of :meth:`__init__` and :meth:`set_round`. After that, a call to :meth:`next_` is all that's needed. The returned partners start to repeat after :meth:`length` number of calls to :meth:`next_`.
	'''
	
	_r = 0  #: The round number of the next matching to return
	_a = None  #: The agent for which to return partners in the next matching
	_l = None  #: Complete list of agents
	_ms = None  #: All matchings (in one direction only, that is, if edge ``(a,b)`` is part of a matching, edge ``(b,a)`` will never be part of any other matching here). The index is the round number.
	_reverse = False  #: Should *reverse* pairs be produced? That is, for edge ``(a,b)`` assume it actually refers to edge ``(b,a)``.
	MESSAGE_PRINT = '''SpeedSpeedDating:
    round         = {round}
    agent         = {agent}
    agents        = {agents}
    next matching = {matching}
    reverse       = {rev}
    next targets  = {targets}'''  #: Used for formatting the output in :meth:`__str__`
	
	def __init__(self, agent, agents):
		'''
		Set agent and agents.
		
		agents must have an index operator and a length of at least 2 and agent must be in agents.
		
		:param agent: The agent for which partners are to be found each round
		:param agents: The list of all agents including agent
		'''
		
		if not hasattr(agents, '__getitem__'):
			raise TypeError('"agents" needs to be a sequence type.')
		if not agent in agents:
			raise ValueError('"agent" is not in "agents".')
		if len(agents) < 2:
			raise ValueError('"agents" is too short.')
		
		self._a = agent
		self._l = agents
		
		# Construct matchings
		# Some temp variables with short names:
		
		l = self._l
		xs = []  # Aggregator. Contains agent lists, cut and pasted together in assorted ways from the original agent list.
		# The case of 2 agents needs to be handled separately, otherwise empty matchings occur.
		if len(l) == 2:
			xs = [l]
		elif len(l) % 2 == 0:
			# For an even number of agents the solution has m matchings with one symmetry and m matchings with a different symmetry.
			
			m = len(self._l) // 2  # Half the length of the agent list.
			
			for i in range(m):  # Matchings with no idle agents
				# Take a portion from the head of the list and append it to the end
				xs.append(l[i:] + l[:i])
			for i in range(m):  # Matchings with two idle agents
				# Take a portion from the head of the list and add to the end, dropping the first and the 'middle' agent of the resulting list
				xs.append(l[i + 1:i + m] + l[i + m + 1:] + l[:i])
		else:
			# Odd number of agents
			for i in range(len(l)):
				# Drop an agent, prepend the remainder of the list after the dropped agent
				xs.append(l[i + 1:] + l[:i])
		
		# For each of the lists of agents in the elements of 'xs', pair up the agents at the head and foot of the list until the list is empty.
		# NOTE (In the even case) half the elements of 'xs' are shorter (because two agents were removed each), so 'm' can't be used here.
		# How it works: split the agent list and zip the first and the reversed second half.
		self._ms = [list(zip(x[:len(x) // 2], x[len(x) // 2:][::-1])) for x in xs]
		
		#print(xs)
		#print(self._ms)
		
	def __str__(self):
		return self.MESSAGE_PRINT.format(round=self._r, agent=self._a, agents=self._l, matching=list(self._ms[self._r]), reverse=self._reverse, targets=list(self._targets(self._a, self._r)))
	
	def _targets(self, a, r):
		'''
		Return a list of partners.
		
		:param a: The agent for which partners are to be returned
		:param int r: The round for which partners are to be returned
		:returns: A list of targets
		'''
		
		if not self._reverse:
			# If the source of an edge in the matching is a, return the target of the edge
			return (x[1] for x in self._ms[r] if (x[0] == a))
		else:
			# Swap interpretation of 'source' and 'target' from above
			return (x[0] for x in self._ms[r] if (x[1] == a))
	
	def next_(self, round_=-1):
		'''
		Returns a list of partners for the next round.
		
		If the parameter ``round_`` is not used, :meth:`next_` returns the partners iterating through the rounds for each subsequent call to :meth:`next_`. The interal state changes by calling :meth:`next_`.
		
		If the parameter ``round_`` is used, :meth:`next_` returns the partners for the specified round. The interal state is not changed by calling :meth:`next_`.
		
		:param int round_: The round for which the partners are to be returned.
		:returns: A list of partners
		'''
		
		if round_ == -1:
			if self._r < len(self._ms):
				n = self._targets(self._a, self._r)
				
				self._r += 1
				if self._r >= len(self._ms):
					self._r = 0
					self._reverse = not self._reverse
				
				return n
			else:
				# This should never happen, since all code that modifies self._r now prevents self._r from becoming too big.
				raise AssertionError('Round is too big.')
		else:
			# Not the most elegant approach, I know, but the one with no duplicate code given all the rest already being here.
			old_round = self._r
			self.set_round(round_)
			n = self.next_()
			self.set_round(old_round)
			return n
	
	def _set_agent(self, agent):
		'''
		Change the agent for which partners are returned.
		
		:param agent: The new agent
		'''
		
		if agent in self._l:
			self._a = agent
		else:
			raise ValueError('"agent" is not in the list of agents.')
	
	def set_round(self, round_):
		'''
		Set the next round counter.
		
		:param round: The new counter. Zero-indexed.
		'''
		
		if 0 <= round_ and round_ < len(self._ms) * 2:
			self._r = round_ % len(self._ms)
			self._reverse = len(self._ms) <= round_
		else:
			raise ValueError('Round is out of range.')
	
	def length(self):
		'''
		Get the number of matchings.
		
		:returns: The number of matchings.
		'''
		
		return len(self._ms) * 2


class Message(object):
	'''
	Generate and validate messages.
	
	All messages XMPProbe exchanges between accounts are generated by instances of this class.
	
	A message (serialized as JSON) contains the following information:
	
	========== ========= ===================================================
	Field Name Type      Interpretation
	========== ========= ===================================================
	magic      string    A constant used to identify a message as a representation of an instance of :class:`Message`.
	version    integer   Version of the message format.
	nonce      integer   An integer number picked randomly at the start of an exchange of messages. Incremented (modulo 0x10000000000) for every further message in the same thread.
	t_sent     ISO 8601  Timestamp when message was generated.
	t_rcvd     ISO 8601  Timestamp when the message to which this message is a reply to was received.
	report     string    The function (purpose) of this message.
	comment    string    A human-readable string that is not processed. Intended to message nosy XMPP server administrators that check what apparent spam XMPProbe bots send around.
	checksum   integer   A checksum of all the above data fields (excluding the checksum field).
	========== ========= ===================================================
	
	The message exchanges that XMPProbe conducts work in the following way:
	
	One agent sends an *ask* message, and the receiving agent responds with an *answer* message.
	
	:note: The checksumming is performed on the string representation of python attributes/objects and not the JSON string representation of the attributes/objects.
	
	*Ask* message specification:
	
	========== =============================================================
	Field Name Value
	========== =============================================================
	magic      'xmpP'
	version    1
	nonce      uniformly distributed random number in the range [0x0, 0xffffffffff] (corresponds to 40 bits of entropy)
	t_sent     Timestamp when message was generated in UTC including timezone information (Example: '2014-02-06T06:34:49.211885+00:00')
	t_rcvd     None
	report     'ping'
	comment    refer to the value of :attr:`COMM`
	checksum   ``zlib.adler32(str(magic) + str(version) + str(nonce) + str(t_sent) + str(t_rcvd) + str(report) + str(comment))``
	========== =============================================================
	
	*Answer* message specification:
	
	========== =============================================================
	Field Name Value
	========== =============================================================
	magic      'xmpP'
	version    1
	nonce      ``((nonce of *ask* message) + 1) % 0x10000000000``
	t_sent     Timestamp when message was generated in UTC including timezone information
	t_rcvd     ``t_sent of *ask* message``
	report     'pong'
	comment    refer to the value of :attr:`COMM`
	checksum   ``zlib.adler32(str(magic) + str(version) + str(nonce) + str(t_sent) + str(t_rcvd) + str(report) + str(comment))``
	========== =============================================================
	
	Usage example:
	
	Alice wants to start a conversation with Bob, so she creates a message object. ::
	
		>>> a = xmpprobe.Message()
		
	
	When she is ready, she generates the message. Since the message contains a timestamp, it should then actually be sent out as soon as possible. ::
	
		>>> a.ask()
		'{"nonce":144974668543,"comment":"XMPP monitoring project. Contact mail:xmpprobe@example.com for information.","t_rcvd":null,"version":1,"magic":"xmpP","report":"ping","checksum":2065312730,"t_sent":"2014-02-19T18:38:55.137526+00:00"}'
	
	Now, Alice sends this message to Bob. Bob receives this message as ``m``. ::
	
		>>> try:
		... 	b = xmpprobe.Message(m)
		... except ValueError as e:
		... 	print('fail!')
	
	Alternatively, if Bob wants to reuse a previously created Message object, he can instead use: ::
	
		>>> try:
		... 	b.decode(m)
		... except ValueError as e:
		... 	print('fail!')
	
	If either way of parsing the message does not raise an error, the message has the correct syntax and meaningful values. Bob can now generate an answering message. Again, because this answer contains a timestamp, it should be sent out as soon as possible. ::
	
		>>> b.answer()
		'{"nonce":144974668544,"comment":"XMPP monitoring project. Contact mail:xmpprobe@example.com for information.","t_rcvd":"2014-02-19T18:38:55.137526+00:00","version":1,"magic":"xmpP","report":"pong","checksum":2954899668,"t_sent":"2014-02-19T18:39:09.088472+00:00"}'
	
	On receiving Bob's answering message as ``m``, Alice can now verify it as being a correct *answer* message to the *ask* message she sent Bob earlier. ::
	
		>>> try:
		... 	a.mark(m)
		... except ValueError as e:
		... 	print('fail!')
	
	If no error is raised, the *answer* message was valid.
	'''
	
	MAGIC = 'xmpP'  #: magic field
	VER = 1  #: version field
	_nonce = -1  #: nonce field
	_t_sent = None  #: A datetime whereof the t_sent field is derived
	_t_rcvd = None  #: A datetime whereof the t_rcvd field is derived
	_report = ''  #: report field
	COMM = 'XMPP monitoring project. Contact mail:xmpprobe@example.com for information.'  #: comment field
	_msg = ''  #: The JSON representation of the message.
	
	#: JSON Schema for message validation (both *ask* and *answer* messages)
	SCHEMA = \
		{
			"type": "object",
			"required": ["magic", "version", "nonce", "t_sent", "t_rcvd", "report", "comment", "checksum"],
			"properties": {
				"magic": {
					"type": "string",
					"enum": ["xmpP"]
				},
				"version": {
					"type": "integer",
					"enum": [1]
				},
				"nonce": {
					"type": "integer",
					"minimum": 0,
					"maximum": 1099511627775
				},
				"t_sent": {
					"type": "string",
					"format": "date-time"
				},
				"t_rcvd": {
					"type": ["string", "null"],
					"format": "date-time"
				},
				"report": {
					"type": "string",
					"enum": ["ping", "pong"]
				},
				"comment": {
					"type": ["string", "null"],
				},
				"checksum": {
					"type": "integer",
					"minimum": 0,
					"maximum": 4294967296
				}
			}
		}
	
	MESSAGE_PRINT = '''Message:
    magic    = {magic}
    version  = {ver}
    nonce    = {nonce}
    t_sent   = {sent}
    t_rcvd   = {rcvd}
    report   = {rep}
    comment  = {comm}
    message  = {msg}'''  #: Used for formatting the output in :meth:`__str__`
	
	def __init__(self, msg=None):
		'''
		If msg=None, class attributes are set to something illegal, otherwise msg is verified and the class attributes are set accordingly.
		
		:param str msg: The message to initialize this instance with.
		'''
		
		if msg is not None:
			self.decode(msg)
		else:
			# There is nothing to initialize, everything happens in self.ask().
			pass
	
	def __str__(self):
		try:
			t_sent = self._t_sent.isoformat()
		except:
			t_sent = None
		try:
			t_rcvd = self._t_rcvd.isoformat()
		except:
			t_rcvd = None
		return self.MESSAGE_PRINT.format(magic=self.MAGIC, ver=self.VER, nonce=self._nonce, sent=t_sent, rcvd=t_rcvd, rep=self._report, comm=self.COMM, msg=self._msg)
	
	def decode(self, msg):
		'''
		Validate a message and store it.
		
		Set class attributes according to message content if the message is valid.
		
		:param str msg: The message to validate.
		
		:raises ValueError:
		'''
		
		d = json.loads(msg)
		jsonschema.validate(d, self.SCHEMA)
		
		magic = d['magic']
		version = d['version']
		nonce = d['nonce']
		t_sent = d['t_sent']  # Is now string, needs to be converted.
		t_rcvd = d['t_rcvd']  # Is now string (or none), may need to be converted.
		report = d['report']
		comment = d['comment']
		checksum = d['checksum']
		
		to_csum = ''.join((magic, str(version), str(nonce), t_sent, str(t_rcvd), report, comment))
		if checksum != zlib.adler32(bytes(to_csum, 'UTF-8')):
			raise ValueError('Checksum is wrong.')
		
		try:
			t_sent = dateutil.parser.parse(t_sent)
		except TypeError:
			raise ValueError('Decoding t_sent has failed.')
		
		# t_rcvd is None for ping messages, and a valid ISO datetime for pong messages.
		if report == 'ping':
			if t_rcvd is not None:
				raise ValueError('t_rcvd is not None.')
		else:
			try:
				t_rcvd = dateutil.parser.parse(t_rcvd)
			except TypeError:
				raise ValueError('Decoding t_rcvd has failed.')
		
		self._nonce = nonce
		self._t_sent = t_sent
		self._t_rcvd = t_rcvd
		self._report = report
		self._msg = msg
		return
	
	def ask(self):
		'''
		Generate *ask* message.
		
		:returns: The *ask* message serialized as JSON.
		'''
		
		self._nonce = random.randint(0x0, 0xffffffffff)
		self._t_sent = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC'))
		self._t_rcvd = None
		self._report = 'ping'
		return self._make_msg()
	
	def _make_msg(self):
		'''
		Calculate checksum and serialize message as JSON.
		
		All used class attributes have to be set to something appropriate before calling this function.
		
		:returns: The message serialized as JSON.
		'''
		
		magic = self.MAGIC
		version = self.VER
		nonce = self._nonce
		t_sent = self._t_sent.isoformat()
		t_rcvd = None
		try:
			t_rcvd = self._t_rcvd.isoformat()
		except AttributeError:
			pass
		report = self._report
		comment = self.COMM
		to_csum = ''.join((magic, str(version), str(nonce), t_sent, str(t_rcvd), report, comment))
		checksum = zlib.adler32(bytes(to_csum, 'UTF-8'))
		
		msg = json.dumps(
			{
				'magic': magic,
				'version': version,
				'nonce': nonce,
				't_sent': t_sent,
				't_rcvd': t_rcvd,
				'report': report,
				'comment': comment,
				'checksum': checksum
			}, separators=(',', ':'))
		self._msg = msg
		return msg
	
	def answer(self):
		'''
		Generate *answer* message.
		
		:returns: The *answer* message serialized as JSON.
		'''
		
		if self._nonce == -1:
			raise ValueError('Answer can not be generated, nonce has illegal value.')
		if self._t_sent is None:
			raise ValueError('Answer can not be generated, t_sent is None.')
		
		self._nonce = (self._nonce + 1) % 0x10000000000
		self._t_rcvd = self._t_sent
		self._t_sent = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC'))
		self._report = 'pong'
		return self._make_msg()
	
	def mark(self, msg):
		'''
		Validate that msg is a correct answer to the message represented by this instance's state.
		
		:param str msg: The message to validate.
		
		:raises ValueError:
		'''
		
		# NOTE: MAGIC, VER, report, COMM and digest are validated by decode() and jsonschema.
		m = Message(msg)
		if self._nonce + 1 != m.nonce:
			raise ValueError('Nonces are not consecutive.')
		if self._t_sent > m.t_sent or self._t_sent != m.t_rcvd:
			raise ValueError('Timestamps are wrong.')
		return
	
	def _debug_init(self, magic, version, nonce, t_sent, t_rcvd, report, comment, message):
		'''Set all internal attributes.'''
		self.MAGIC = magic
		self.VER = version
		self._nonce = nonce
		self._t_sent = t_sent
		self._t_rcvd = t_rcvd
		self._report = report
		self.COMM = comment
		self._msg = message
	
	@property
	def nonce(self):
		'''Get the nonce.'''
		return self._nonce
	
	@property
	def t_sent(self):
		'''Get the sent timestamp.'''
		return self._t_sent
	
	@property
	def t_rcvd(self):
		'''Get the received timestamp.'''
		return self._t_rcvd
	
	@property
	def report(self):
		'''Get the report.'''
		return self._report


class DataLogger(logging.Logger):
	'''
	A logger that outputs CSV.
	
	This logger generates its messages using Python's CSV module, and has no fancy log string formatting. Only the content of the iterable passed to :meth:`writerow` will be written to the logfile.
	'''
	
	def __init__(self, file_name):
		'''
		:param str file_name: The log filename.
		'''
		
		logging.Logger.__init__(self, 'DataLogger', level=logging.DEBUG)
		fileHandler = logging.FileHandler(file_name)
		fileFormatter = logging.Formatter('%(message)s')
		fileHandler.setFormatter(fileFormatter)
		fileHandler.setLevel(logging.DEBUG)
		self.addHandler(fileHandler)
		
		self._strio = io.StringIO()
		self._writer = csv.writer(self._strio, quoting=csv.QUOTE_MINIMAL)
	
	def writerow(self, data, lvl=logging.DEBUG):
		'''
		Produce one logfile record.
		
		:param data: An iterable of fields. This will be converted to a CSV row, and then written to file.
		:param lvl: Logging level. See documentation of the logging module for information on levels.
		'''
		
		if hasattr(data, '__getitem__'):
			strio = io.StringIO()
			writer = csv.writer(strio, quoting=csv.QUOTE_MINIMAL)
			writer.writerow(data)
			self.log(lvl, strio.getvalue().rstrip('\r\n'))
		else:
			raise ValueError('"data" has no "__getitem__"')


class EchoBot(sleekxmpp.ClientXMPP):
	'''
	A simple SleekXMPP bot that will echo messages it receives.
	
	EchoBot class from SleekXMPP: The Sleek XMPP Library; Copyright (C) 2010  Nathanael C. Fritz
	'''
	
	def __init__(self, jid, password):
		'''
		:param str jid: Jabber ID (bare or full) of the account to be used
		:param str password: Password for the account
		'''
		
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self.add_event_handler('session_start', self.start)
		self.add_event_handler('message', self.message)
	
	def start(self, event):
		'''Event handler for ``session_start``.'''
		self.send_presence()
		self.get_roster()
	
	def message(self, msg):
		'''Event handler for ``message``.'''
		if msg['type'] in ('chat', 'normal'):
			msg.reply(msg['body']).send()


class AnswerBot(sleekxmpp.ClientXMPP):
	'''
	A simple message-answering bot.
	
	AnswerBot waits for *ask* messages (see :class:`Message`). When it receives one, AnswerBot responds with the appropriate *answer* message.
	'''
	
	_l = None  #: Logger instance
	
	def __init__(self, jid, password):
		'''
		:param str jid: Jabber ID (bare or full) of the account to be used
		:param str password: Password for the account
		'''
		
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self._l = logging.getLogger('default')
		self.add_event_handler('session_start', self.start)
		self.add_event_handler('message', self.message)
	
	def start(self, event):
		'''Event handler for ``session_start``.'''
		self.send_presence()
		self.get_roster()
		self._l.info('%s started.' % type(self).__name__)
	
	def message(self, msg):
		'''Event handler for ``message``.'''
		if msg['type'] in ('chat', 'normal'):
			t = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
			m = msg['body']
			try:
				b = Message(m)
			except ValueError:
				self._l.warning('The validation of the received message failed. The message was "%s"' % m)
				return
			self._l.info('A valid message with the nonce {nonce} was received at time {time}'.format(nonce=b.nonce, time=t))
			am = b.answer()
			self._l.debug('Sending reply "{}"'.format(am))
			msg.reply(am).send()
		else:
			self._l.debug('Received message of unknown type.')


class AskBot(sleekxmpp.ClientXMPP):
	'''
	A bot that sends a message and waits for a reply.
	
	AskBot sends an *ask* message (see :class:`Message`) and waits for an *answer* message in reply.
	'''
	
	_l = None  #: Logger instance
	_msg = None  #: Instance of :class:`Message`; sent message
	_buddy = ''  #: JID of partner to send message to
	
	def __init__(self, jid, password, buddy):
		'''
		:param str jid: Jabber ID (bare or full) of the account to be used
		:param str password: Password for the account
		:param str buddy: Jabber ID (bare or full) of the account to send the message to
		'''
		
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self._l = logging.getLogger('default')
		
		self._buddy = buddy
		self._msg = Message()
		
		self.add_event_handler('session_start', self.start)
		self.add_event_handler('message', self.message)
	
	def start(self, event):
		'''Event handler for ``session_start``.'''
		self.send_presence()
		self.get_roster()
		self._l.info('%s started.' % type(self).__name__)
		
		m = self._msg.ask()
		self._l.info('Sending message "{msg}" to {buddy}.'.format(msg=m, buddy=self._buddy))
		self.send_message(mto=self._buddy, mbody=m, mtype='chat')
	
	def message(self, msg):
		'''Event handler for ``message``.'''
		if msg['type'] in ('chat', 'normal'):
			t = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
			m = msg['body']
			try:
				self._msg.mark(m)
			except ValueError:
				self._l.warning('The validation of the received message failed. The message was "%s"' % m)
				return
			self._l.info('A valid message with the nonce {nonce} was received at time {time}'.format(nonce=self._msg.nonce(), time=t))
			self.disconnect(wait=True)
		else:
			self._l.debug('Received message of unknown type.')


class TimeBot(sleekxmpp.ClientXMPP):
	'''
	A bot that connects to a Jabber account an periodically generates log entries.
	
	TimeBot periodically logs the wall clock time. It waits for a random interval within each time slot to spice things up.
	
	This class is for experimentation with scheduling.
	
	:note: Timing errors will occur, and they accumulate.
	'''
	
	_l = None  #: Logger instance
	
	def __init__(self, jid, password):
		'''
		:param str jid: Jabber ID (bare or full) of the account to be used
		:param str password: Password for the account
		'''
		
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self._l = logging.getLogger('default')
		
		self.add_event_handler('session_start', self.start)
	
	def start(self, event):
		'''Event handler for ``session_start``.'''
		self.send_presence()
		self.get_roster()
		self._l.info('%s started.' % type(self).__name__)
		
		self.schedule('first timer', 5, self.timer, args=((0, 1), ), repeat=True)
		self.schedule('second timer', 5, self.timer, args=((2, 3), ), repeat=True)
	
	def timer(self, random_interval=(0, 1)):
		'''Event handler for timer.'''
		t0 = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
		i = random_interval
		r = random.uniform(i[0], i[1])
		time.sleep(r)
		self._l.debug('This is timer at {time}. I waited an extra {wait}s before telling you.'.format(time=t0, wait=r))


class PeriodicBot(sleekxmpp.ClientXMPP):
	'''
	A bot that connects to a Jabber account an periodically generates log entries.
	
	:class:`PeriodicBot` is similar to :class:`TimeBot`, but :class:`PeriodicBot` accounts for errors in time periods that might accumulate. That is, variation in period length is corrected in the subsequent period.
	
	The *ticks* when a period starts are chosen such that a tick falls on the Unix time epoch. Due to this adjustment of time ticks, the first timer may fire as late as `MIN_PREP`` :math:`+` round(period) seconds after :meth:`__init__` is called.
	
	Note that for keeping track of time integer arithmetic is used internally, so period is rounded to an integer number.
	'''
	
	_l = None  #: Logger instance
	_t = 0  #: The last returned tick
	_gt = None  #: GoodTimes instance
	_p = 0  #: The period
	
	def __init__(self, jid, password, period):
		'''
		:param str jid: Jabber ID (bare or full) of the account to be used
		:param str password: Password for the account
		:param number period: Period for the timer. Will be rounded to an integer.
		'''
		
		sleekxmpp.ClientXMPP.__init__(self, jid, password)
		self._l = logging.getLogger('default')
		self._gt = GoodTimes(period, 5)
		self._p = period
		
		self.add_event_handler('session_start', self._start)
	
	def _start(self, event):
		'''Event handler for ``session_start``.'''
		self.send_presence()
		self.get_roster()
		self._l.info('%s started.' % type(self).__name__)
		
		self._set_timer()
	
	def _set_timer(self):
		'''Arm timer.'''
		delta = self._gt.delta()
		self._l.debug('This period is the {}th period after the last one'.format(self._gt.periods_incremented))
		self.schedule('period', delta, self._timer, args=(delta,), repeat=False)
	
	def _timer(self, d):
		'''Event handler for timer.'''
		t = time.time()
		self._l.debug('Now: {time}. GoodTimes @: {gtf}. Waited for: {delta}s. Phase: {phase}.'.format(time=t, gtf=self._gt.last_period, delta=d, phase=self._gt.phase()))
		r = random.uniform(0, self._p * 2)
		time.sleep(r)
		self._l.debug('Slept for another {}s.'.format(r))
		self._set_timer()


class GoodTimes(object):
	'''
	Get times deltas for scheduling periodic events.
	
	This class helps schedule events that happen at exactly :math:`n \\cdot p` n natural and p the period (an integer number of seconds) referenced to unix time epoch.
	
	When using these times to go through matchings of a solution to the SSD problem, the function :meth:`phase` can be used to determine which matching should be used in the next time slot. :meth:`phase` will return 0 every :math:`l`\ th time period starting with 0. :math:`l` is the number of matchings in the solution of the SSD problem.
	'''
	
	_p = 0  #: The period
	_l = 0  #: The number of periods until the phase has the same value again
	_last = 0  #: Timestamp of the period referred to by the return value of :meth:`delta` last. May be in the future.
	_inc = 0  #: The number of periods incremented when _last was last incremented.
	
	def __init__(self, period, length=1):
		'''
		:param int period: The period.
		:param int length: The number of matchings in the solution of the SSD problem
		'''
		
		self._p = abs(round(period))
		self._l = abs(round(length))
		#self._last = ((math.trunc(time.time()) // self._p) + 1) * self._p
		self._last = 0
	
	def delta(self):
		'''Return the time remaining until the next whole multiple of the period since epoch.'''
		t = time.time()
		l = self._last
		#print('self._last: {}'.format(self._last))
		self._last = ((math.trunc(t) // self._p) + 1) * self._p
		#print('new self._last: {}'.format(self._last))
		self._inc = (self._last - l) // self._p
		return self._p - (t % self._p)
	
	@property
	def last_period(self):
		'''Timestamp of the period that the last returned :meth:`delta` led up to.'''
		return self._last
	
	@property
	def periods_incremented(self):
		'''
		Returns the number of periods the last returned delta counts down to compared to the last but one delta.
		
		For the first call after instantiation the number returned will generally be very big, since the 'last' returned timestamp will be set to epoch.
		'''
		
		return self._inc
	
	def phase(self):
		'''
		Return the phase within the matchings of a solution to the SSD problem for the period starting in the future from when the last call to :meth:`delta` was made.
		
		This can be used as the argument to :meth:`set_round` of :class:`SpeedSpeedDating`.
		'''
		
		return (self._last % (self._p * self._l)) // self._p


class NormalBot(sleekxmpp.ClientXMPP):
	'''
	:class:`NormalBot` periodically sends messages to its partners (picking partners according to a solution of the Speed Speed Dating problem; see :class:`SpeedSpeedDating`) and responds to messages.
	
	The *ticks* when a period starts are chosen such that a tick falls on the Unix time epoch. Due to this adjustment of time ticks, the first timer may fire as late as `MIN_PREP`` :math:`+` round(period) seconds after :meth:`__init__` is called.
	
	Note that for keeping track of time integer arithmetic is used internally, so period is rounded to an integer number.
	'''
	
	_PP = None  #: Instance of pprint
	_l = None  #: Logger instance
	_dl = None  #: DataLogger instance
	_ssd = None  #: :class:`SpeedSpeedDating` instance
	_gt = None  #: :class:`GoodTimes` instance
	
	_c = None  #: The configuration (originally parsed from the config file)
	_p = 0  #: The period in seconds.
	_r = ''  #: Jabber resource to use for all JIDs
	_a = ''  #: My full JID
	
	_m = {}  #: Dictionary of message objects: *key* is the JID and value is an instance of :class:`Message`. The last message sent to each JID in turn is stored here.
	_try_reconnect_next = False  #: If account has been disconnected, try to reconnect again in the next period.
	MESSAGE_PRINT = '''========== Configuration: ==========
	{c}
	========== /Configuration ==========
	========== _m: ==========
	{m}
	========== /_m ==========
	Period: {p}
	Resource: {r}
	Me: {a}'''  #: Used for formatting the output in __str__
	
	def __str__(self):
		return self.MESSAGE_PRINT.format(c=self._PP.pformat(self._c), m=self._PP.pformat(self._m), p=self._p, r=self._r, a=self._a)
	
	def __init__(self, account, resource, config):
		'''
		:param str account: The JID of the account to be used.
		:param str resource: The resource name to use for this instance and contacting all other jabber accounts.
		:param config: The object that represents the parsed config file. Used to extract information such as the period and the JIDs of all other accounts.
		'''
		
		self._PP = pprint.PrettyPrinter(indent=4)
		self._l = logging.getLogger('default')
		self._dl = DataLogger(config['datafile'])
		# Use this for logging: self._dl.writerow([NormalBot._dl_t(), self._a, ''])
		
		self._c = config
		self._p = config['period']
		self._r = resource
		self._a = add_resource(account, resource)
		self._try_reconnect_next = False
		
		accounts = [x['jid'] for x in config['accounts'] if x['usage'] == 'normal']
		accounts.sort()  # Make the order of accounts deterministic, so that every instance of :class:`NormalBot` will have the same results for the SSD problem.
		accounts = list(map(lambda x: add_resource(x, resource), accounts))
		self._l.debug('This list is passed to SSD as "accounts":\n{}.\nI am {}.'.format(self._PP.pformat(accounts), self._a))
		self._ssd = SpeedSpeedDating(self._a, accounts)
		for i in accounts:
			self._m[i] = None
		self._l.debug('This is the list of Message objects. There should be one for each account, and they should all be None.\n{}'.format(self._PP.pformat(self._m)))
		
		self._gt = GoodTimes(self._p, self._ssd.length())
		
		self._l.debug('Getting account config for account {} and config {}.'.format(account, self._PP.pformat(config)))
		ac = get_account_config(account, config)
		sleekxmpp.ClientXMPP.__init__(self, self._a, ac['password'])
		
		self.auto_authorize = True  # Subscription requests will be automatically accepted (a 'subscribed' response will be sent).
		self.auto_subscribe = False  # 'subscribed' responses will be followed up by sending a 'subscribe' request in turn.
		
		self.add_event_handler('session_start', self._start)
		self.add_event_handler('failed_auth', self._failed_auth)
		self.add_event_handler('disconnected', self._disconnected)
		
		self.add_event_handler('presence_subscribe', self._subscribe)
		self.add_event_handler('presence_subscribed', self._subscribed)
		#self.add_event_handler('roster_update', self._roster_update) # Not needed, is handled by ClientXMPP._handle_roster
		
		self.add_event_handler('message', self._message)
	
	def _start(self, event):
		'''
		Event handler for ``session_start``.
		
		Starts the timer.
		'''
		
		self._dl.writerow([NormalBot._dl_t(), self._a, 'connect'])
		
		self._try_reconnect_next = False
		
		self.send_presence()
		self.get_roster()
		self._l.debug('%s started.' % type(self).__name__)
		
		#roster = self.client_roster
		#print(type(roster))
		roster = []
		for k in self.client_roster:
			roster.append((k, self.client_roster[k]['subscription']))
		roster = self._PP.pformat(roster)
		self._l.debug('This is my roster:\n{}'.format(roster))
		
		self._set_timer()
	
	def _failed_auth(self, event):
		'''Event handler for ``failed_auth``.'''
		self._dl.writerow([NormalBot._dl_t(), self._a, 'nauth'])
		self._l.info('Auth failed at {}.'.format(time.time()))
	
	def _disconnected(self, event):
		'''Event handler for ``disconnected``.'''
		self._dl.writerow([NormalBot._dl_t(), self._a, 'disconnect'])
		self._l.info('I was disconnected at {}.'.format(time.time()))
		self._try_reconnect_next = True
	
	def _subscribe(self, presence):
		'''
		Event handler for ``presence_subscribe``.
		
		:note: This even handler should never be called, as self.auto_authorize == True.
		'''
		
		p = presence['from']
		
		self._dl.writerow([NormalBot._dl_t(), self._a, 'rsubscribe', p])
		self._l.critical('presence_subscribe handler was called for a presence notification from {}.'.format(presence['from']))
	
	def _subscribed(self, presence):
		'''
		Event handler for ``presence_subscribed``.
		
		A call to this method means that a partner has accepted a presence subscription request. He shall be added to the roster appropriately.
		'''
		
		# FIXME Check if full or bare JIDs are used here.
		p = presence['from']
		
		self._dl.writerow([NormalBot._dl_t(), self._a, 'rsubscribed', p])
		
		if self.client_roster[p]['subscription'] == 'from':
			self.update_roster(p, subscription='both')
			self._l.info('presence_subscribed handler: {} is now both.'.format(p))
		elif self.client_roster[p]['subscription'] == 'none':
			self.update_roster(p, subscription='to')
			self._l.info('presence_subscribed handler: {} is now to.'.format(p))
		else:
			self._l.error('presence_subscribed handler: {} sent "subscribed", but there\'s nothing to do.'.format(p))
			return
		self.send_presence(pto=p)
	
	def _message(self, msg):
		'''Event handler for ``message``.'''
		start_t = NormalBot._dl_t()
		if msg['type'] in ('chat', 'normal'):
			t = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
			m = msg['body']
			buddy = msg['from']  # It's a full JID.
			self._l.debug('Received message\n{msg}\nfrom {buddy}.'.format(msg=m, buddy=buddy))
			try:
				b = Message(m)
			except ValueError:
				self._l.debug('The validation of the received message failed. The message was "%s"' % m)
				self._dl.writerow([start_t, self._a, 'runknown', msg['from'], m])
				return
			self._l.debug('A valid message with the nonce {nonce} was received at time {time}'.format(nonce=b.nonce, time=t))
			if b.report == 'ping':
				# It's an *ask* message, reply to it.
				ask_nonce = b.nonce
				ask_sent = b.t_sent
				self._dl.writerow([start_t, self._a, 'rask', buddy, ask_nonce, ask_sent])
				am = b.answer()
				self._l.debug('Sending reply "{}"'.format(am))
				msg.reply(am).send()
				self._dl.writerow([NormalBot._dl_t(), self._a, 'sanswer', buddy, ask_nonce, ask_sent])
			else:
				try:
					if self._m[buddy]:
						try:
							self._m[buddy].mark(m)
						except ValueError:
							surp = Message(m)
							self._l.debug('The message was not the expected answer.')
							self._dl.writerow([start_t, self._a, 'rsurprise', buddy, NormalBot._nonce_decr(surp.nonce), surp.t_rcvd, surp.t_sent])
							return
						self._l.debug('The validation of the received message was successful. The message was "%s"' % m)
						ranswer = Message(m)
						self._dl.writerow([start_t, self._a, 'ranswer', buddy, self._m[buddy].nonce, self._m[buddy].t_sent, ranswer.t_sent])
					else:
						self._l.debug('There is no stored ask Message for buddy {}.'.format(buddy))
						surp = Message(m)
						self._dl.writerow([start_t, self._a, 'rsurprise', buddy, NormalBot._nonce_decr(surp.nonce), surp.t_rcvd, surp.t_sent])
				except KeyError:
					self._l.debug('Buddy {} was not found in _m.'.format(buddy))
		else:
			self._l.debug('Received message of unknown type.')
			self._dl.writerow([start_t, self._a, 'runknown', msg['from'], msg['body']])
	
	def _set_timer(self):
		'''Arm timer.'''
		delta = self._gt.delta()
		pi = self._gt.periods_incremented
		self._l.debug('This period is the {}th period after the last one'.format(pi))
		if pi > 1 and pi < 32000000:
			# Because of how GoodTimes internally intitializes, there will be one very big periods_incremented at the start. This is filtering for that case.
			# The upper limit is assuming that XMPProbe does not hang for 1 year (with a 1 second periond) and then suddenly recover. If it does, you're gonna have a bad time.
			# FIXME Double-check this
			start_t = NormalBot._dl_t()
			# For each missed round, that is (pi-1) times, get the next buddies from _ssd
			while pi > 1:
				for b in list(self._ssd.next_()):
					self._dl.writerow([start_t, self._a, 'skip', b])
				pi -= 1
		self.schedule('period', delta, self._timer, args=(delta,), repeat=False)
	
	def _timer(self, d):
		'''
		Event handler for timer.
		
		Reconnect when disconnected. When connected, send out *ask* messages according to the SSD solution.
		
		:param float d: Time waited before timer fired. Not technically necessary, but useful for debugging.
		'''
		
		# If I'm disconnected, attempt to connect, and do nothing else.
		if self._try_reconnect_next:
			self.connect()
			self.process(threaded=True)
		else:
			t = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
			self._l.debug('This is timer at {time}. I had to wait for {delta}s before firing.'.format(time=t, delta=d))
			self._ssd.set_round(self._gt.phase())
			buddies = list(self._ssd.next_())
			self._l.debug('SSD says "{}" are my next buddies.'.format(self._PP.pformat(buddies)))
			for b in buddies:
				if not add_resource(b) in self.client_roster:
					self._l.debug('I am adding {} as a buddy.'.format(b))
					self.send_presence(pto=b, ptype='subscribe')
					self._dl.writerow([NormalBot._dl_t(), self._a, 'ssubscribe', b])
				else:
					self._m[b] = Message()
					m = self._m[b].ask()
					self._l.debug('Sending message "{msg}" to {buddy}.'.format(msg=m, buddy=b))
					self.send_message(mto=b, mbody=m, mtype='chat')
					self._dl.writerow([NormalBot._dl_t(), self._a, 'sask', b, self._m[b].nonce, self._m[b].t_sent])
		self._set_timer()
	
	@staticmethod
	def _dl_t():
		'''Generate a timestamp for DataLogger log records.'''
		#return datetime.datetime.utcnow().replace(tzinfo=pytz.timezone('UTC')).isoformat()
		return time.time()
	
	@staticmethod
	def _nonce_decr(nonce):
		'''
		Decrement a nonce.
		
		Given a nonce from an *answer* message, the nonce from the corresponding *ask* message is returned.
		'''
		
		return (nonce + 0xffffffffff) % 0x10000000000


def jid_good(jid):
	'''
	Test if a JID is valid and bare.
	
	:param str jid: The JID.
	:raises: ValueError
	'''
	
	try:
		j = sleekxmpp.xmlstream.JID(jid)
	except sleekxmpp.InvalidJID:
		raise ValueError('The JID "{}" could not be parsed.'.format(jid))
	if not (j.user and j.server):
		raise ValueError('The JID "{}" is missing a user or server part.'.format(jid))
	if j.resource:
		raise ValueError('The JID "{}" is has a resource.'.format(jid))


def validate_config(config):
	'''
	Validate the configuration file passed to XMPProbe.
	
	:param str config: The configuration in JSON.
	:returns: If the configuration file is valid, and object representing the configuration is returned. Otherwise the validator's error is raised.
	'''
	
	#: The schema used for the validation.
	SCHEMA = \
	{
		"type": "object",
		"required": ["loglevel", "stdoutloglevel", "dataloglevel", "logfile", "datafile", "period", "accounts"],
		"properties": {
			"loglevel": {
				"type": "string",
				"enum": ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
			},
			"stdoutloglevel": {
				"type": "string",
				"enum": ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
			},
			"dataloglevel": {
				"type": "string",
				"enum": ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
			},
			"logfile": {
				"type": "string"
			},
			"datafile": {
				"type": "string"
			},
			"period": {
				"type": "integer",
				"minimum": 1,
				"maximum": 60  # FIXME This limit is not necessary at all.
			},
			"accounts": {
				"type": "array",
				"minItems": 1,
				"uniqueItems": True,
				"items": {
					"type": "object",
					"required": ["jid", "password", "usage"],
					"properties": {
						"jid": {
							"type": "string"
						},
						"password": {
							"type": "string"
						},
						"usage": {
							"type": "string",
							"enum": ["answer", "ask", "debug_periodicbot", "debug_timebot", "echo", "fixme", "normal"]
						}
					}
				}
			}
		}
	}
	
	c = json.loads(config)
	
	jsonschema.validate(c, SCHEMA)
	
	# Make sure jid's are unique.
	jids = [x['jid'] for x in c['accounts']]
	unique_jids = set(jids)
	if len(unique_jids) != len(jids):
		raise ValueError('At least one of the JIDs in the "accounts" section of the config file is present more than once.')
	
	# Test that JIDs are actually JIDs
	for jid in jids:
		jid_good(jid)
	
	return c


def get_account_config(account, config):
	'''
	Extract the configuration information for one Jabber account from the "accounts" section of an XMPProbe configuration.
	
	:param str account: The JID for which the configuration is to be retrieved.
	:param config: The object representing the configuration.
	'''
	
	return [x for x in config['accounts'] if x['jid'] == account][0]


def parse_config(file_name):
	'''
	Parse and validate the configuration file for XMPProbe.
	
	:param str file_name: The configuration file name.
	:returns: The parsed configuration as an object.
	'''
	
	config = None  #: # Holds parsed configuration file
	try:
		with open(file_name, 'r') as fh:
			f = fh.read()
			config = validate_config(f)
	except (OSError, IOError) as e:
		print('The configuration file can not be accessed.', file=sys.stderr)
		raise e
	except Exception as e:
		print('The configuration file contains JSON syntax errors.', file=sys.stderr)
		raise ValueError("{0}".format(e), file=sys.stderr)
	return config


def arguments(argv):
	'''
	Parse the command-line arguments to XMPProbe.
	
	:param argv: The command line.
	:returns: The output of ``argparse.ArgumentParser.parse_args``
	'''
	
	parser = argparse.ArgumentParser(description='XMPProbe: Monitoring XMPP Servers.')
	parser.add_argument('account', help='The account configuration to be used by this instance. An object with the "jid" property set to this value must exist in the "accounts" array in the specified configuration file.', type=str)
	parser.add_argument('resource', help='The jabber resource to be used by this instance. Becomes a part of the log file name.', type=str)
	parser.add_argument('-c', '--config', help='The path of the configuration file (default: "./xmpprobe.conf")', type=str, default='./xmpprobe.conf')
	parser.add_argument('-b', '--buddy', help='The JID to send the ask message to. This argument is only evaluated when the "account" is of usage type "ask".')
	args = parser.parse_args(argv)
	
	## Validate command line arguments
	try:
		jid_good(args.account)
		if args.buddy:
			jid_good(args.buddy)
	except ValueError as e:
		raise ValueError('Validating the command line arguments produced the following error:\n{}'.format(str(e)))
	
	return args


def add_resource(jid, resource=None):
	'''
	Add or remove the resource to a full or bare JID.
	
	:param str jid: The JID.
	:param str resource: The resource. If the resource is ``None``, the bare JID will be returned.
	'''
	
	j = sleekxmpp.xmlstream.JID(jid)
	j.resource = resource
	if resource:
		return j.full
	else:
		return j.bare


def main(argv):
	'''Function to be called when run from the command line.'''
	#PP = pprint.PrettyPrinter(indent=4)
	
	## Check Python version
	# NOTE Maybe this isn't a hard requirement... But, I'll just leave this here to make sure that I'm using Python 3 for development and testing. Specifically: Python 3.2.3.
	# From sleek's documentation: 'SleekXMPP must use UTF-8'. When not on Python 3, some magic needs to be used. See: http://sleekxmpp.com/getting_started/echobot.html
	if sys.version_info.major != 3:
		print('You are not running python version 3.x.', file=sys.stderr)
		return Ret.PYTHON_VERSION_FAIL
	
	## Parse arguments
	args = arguments(argv)
	
	## Parse configuration file
	config = parse_config(args.config)
	
	## Check that the argument "account" has a configuration section.
	if not args.account in [x['jid'] for x in config['accounts']]:
		print('The account "{}" (specified on the command line) is not in the config file.'.format(args.account), file=sys.stderr)
		return Ret.ARGUMENT_ERROR
	
	## Extract configuration options for logging.
	loglevel = config['loglevel']
	stdoutloglevel = config['stdoutloglevel']
	#dataloglevel = config['dataloglevel']
	config['logfile'] = config['logfile'].format(account=args.account, resource=args.resource)
	logfile = config['logfile']
	config['datafile'] = config['datafile'].format(account=args.account, resource=args.resource)
	#datafile = config['datafile']
	period = config['period']
	
	## Set up logging
	logger = logging.getLogger('default')
	logger.setLevel(logging.DEBUG)
	
	# NOTE An interesting alternative would be RotatingFileHandler.
	fileHandler = logging.FileHandler(logfile)
	fileFormatter = logging.Formatter('%(created)f,%(threadName)s,%(levelname)s,%(message)s')
	fileHandler.setFormatter(fileFormatter)
	fileHandler.setLevel(loglevel)
	logger.addHandler(fileHandler)
	
	consoleHandler = logging.StreamHandler(sys.stdout)
	consoleFormatter = logging.Formatter('%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s')
	consoleHandler.setFormatter(consoleFormatter)
	consoleHandler.setLevel(stdoutloglevel)
	logger.addHandler(consoleHandler)
	
	# FIXME Now, how would one retrieve the logging level from a handler?
	logger.info('The logging level is set to %s.', logging.getLevelName(logger.getEffectiveLevel()))
	logger.info('Running Python %s.', platform.python_version())
	
	## Extract configuration for the account used by this instance.
	a = get_account_config(args.account, config)
	a = a.copy()
	a['jid'] = add_resource(a['jid'], args.resource)
	
	logger.debug('The account configuration of this instance looks like this: {}'.format(a))
	
	# The available cases are: answer, ask, debug_periodicbot, debug_timebot, echo, fixme, normal
	if a['usage'] == 'answer':
		xmpp = AnswerBot(a['jid'], a['password'])
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	elif a['usage'] == 'ask':
		xmpp = AskBot(a['jid'], a['password'], add_resource(args.buddy, args.resource))
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	elif a['usage'] == 'debug_periodicbot':
		logger.warning('Using debug bot.')
		xmpp = PeriodicBot(a['jid'], a['password'], period)
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	elif a['usage'] == 'debug_timebot':
		xmpp = TimeBot(a['jid'], a['password'])
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	elif a['usage'] == 'echo':
		xmpp = EchoBot(a['jid'], a['password'])
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	elif a['usage'] == 'fixme':
		logger.critical('The config file specifies the usage "fixme" for the requested account.')
		return Ret.ARGUMENT_ERROR
	elif a['usage'] == 'normal':
		xmpp = NormalBot(args.account, args.resource, config)
		xmpp.register_plugin('xep_0030')  # Service Discovery
		xmpp.register_plugin('xep_0199')  # XMPP Ping
		
		if xmpp.connect():
			xmpp.process(block=True)
			logger.info('Done.')
		else:
			logger.critical('Unable to connect.')
	else:
		logger.critical('The config file specifies an unknown usage for account "{}".'.format(args.account))
		return Ret.CONFIG_FILE_ERROR
	
	return Ret.FINE


if __name__ == '__main__':
	# FIXME Is using the '[1:]' _here_ good style?
	sys.exit(main(sys.argv[1:]))
