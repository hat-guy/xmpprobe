About
*****

``XMPProbe`` is a tool for monitoring the availability of XMPP (also known as *Jabber*) servers. To do this, it requires at least one working account on each of the servers to be monitored.

To use XMPProbe a configuration file is required. The specifics of the configuration file are documented in :ref:`conf`.

For XMPProbe, the specification of the command line arguments is printed below: ::

    usage: xmpprobe [-h] [-c CONFIG] [-b BUDDY] account resource

    XMPProbe: Monitoring XMPP Servers.

    positional arguments:
    account               The account configuration to be used by this instance.
                            An object with the "jid" property set to this value
                            must exist in the "accounts" array in the specified
                            configuration file.
    resource              The jabber resource to be used by this instance.
                            Becomes a part of the log file name.

    optional arguments:
    -h, --help            show this help message and exit
    -c CONFIG, --config CONFIG
                            The path of the configuration file (default:
                            "./xmpprobe.conf")
    -b BUDDY, --buddy BUDDY
                            The JID to send the ask message to. This argument is
                            only evaluated when the "account" is of usage type
                            "ask".

For the use in production, only the arguments ``--config``, ``account`` and ``resource`` are of use.

Invocation example: ::

    $ xmpprobe -c config/xmpprobe-config.json "test0@example.com" "test" &
    $ xmpprobe -c config/xmpprobe-config.json "test1@example.com" "test" &
    $ xmpprobe -c config/xmpprobe-config.json "test2@example.com" "test" &
    $ xmpprobe -c config/xmpprobe-config.json "test3@example.com" "test" &

Each Jabber account that should be monitored has to run its own instance of xmpprobe. Each account also needs an appropriate configuration section in the config file.

The typical use case is to start one instance of XMPProbe for every account with usage type ``normal`` in the configuration file.

The individual instances should be started as close together as possible, but this is not required.

Note that no chat messages might be exchanged for up to (number of accounts) :math:`\cdot` (period) seconds, that is, XMPProbe can have a long initialization phase.

Return codes of XMPProbe are documented in the class :class:`Ret`. XMPProbe should only quit on its own right at the start. After the configuration is deemed to be valid, it runs indefinitely.