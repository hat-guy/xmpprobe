XMPProbe Module
***************

.. automodule:: xmpprobe
.. autofunction:: add_resource
.. autofunction:: arguments
.. autofunction:: get_account_config
.. autofunction:: jid_good
.. autofunction:: main
.. autofunction:: parse_config
.. autofunction:: validate_config

Ret
===

.. autoclass:: Ret
   :members:
   :private-members:
   :undoc-members:

SpeedSpeedDating
================

.. autoclass:: SpeedSpeedDating
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

Message
=======

.. autoclass:: Message
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

DataLogger
==========

.. autoclass:: DataLogger
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

EchoBot
=======

.. autoclass:: EchoBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

AnswerBot
=========

.. autoclass:: AnswerBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

AskBot
======

.. autoclass:: AskBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

TimeBot
=======

.. autoclass:: TimeBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

PeriodicBot
===========

.. autoclass:: PeriodicBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

GoodTimes
=========

.. autoclass:: GoodTimes
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__

NormalBot
=========

.. autoclass:: NormalBot
   :members:
   :private-members:
   :undoc-members:
   
   .. automethod:: __init__