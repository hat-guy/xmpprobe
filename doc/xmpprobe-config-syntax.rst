.. _conf:

XMPProbe Configuration File Format
**********************************

XMPProbe requires a correct configuration file to run. All the settings that are not likely to change between different runs of XMPProbe can only be set by options in the configuration file, and not by command-line arguments.

The configuration file is written in JSON.

Sample configuration
====================

The following example is a complete and valid configuration file. The semantics of the individual parameters are explained below. ::

    {
        "loglevel": "DEBUG",
        "stdoutloglevel": "DEBUG",
        "dataloglevel": "DEBUG",
        "logfile": "./logs/xmpprobe-{account}-{resource}.log",
        "datafile": "./logs/xmpprobe-{account}-{resource}.csv",
        "period": 3,
        "accounts": [
            {
                "jid": "test0@example.com",
                "password": "secret0",
                "usage": "normal"
            },
            {
                "jid": "test1@example.com",
                "password": "secret1",
                "usage": "normal"
            }
        ]
    }

Configuration Parameters
========================

The configuration is represented by one JSON object. This object must have all of the following keys:

loglevel (string):
    The logging level for the log data written to general *logfile*. This key must have one of the following values: ``DEBUG``, ``INFO``, ``WARNING``, ``ERROR``, ``CRITICAL``.

stdoutloglevel (string):
    The logging level for the log data written to standard output. This key must have one of the following values: ``DEBUG``, ``INFO``, ``WARNING``, ``ERROR``, ``CRITICAL``.

dataloglevel (string):
    The logging level for the data written to the CSV files. This key must have one of the following values: ``DEBUG``, ``INFO``, ``WARNING``, ``ERROR``, ``CRITICAL``.
    
    Note that the only sensible value to currently use is ``DEBUG``, this may change in future versions.

logfile (string):
    The schema for the path and filename of the general log file. This string must include the substrings ``{account}`` (which will be replaced by the Jabber ID of the used account) and ``{resource}`` (which will be replaced by the resource name used). These two parameters are command-line arguments.

datafile (string):
    The schema for the path and filename of the data file (CSV format). This string must include the substrings ``{account}`` (which will be replaced by the Jabber ID of the used account) and ``{resource}`` (which will be replaced by the resource name used). These two parameters are command-line arguments.

period (integer):
    The sampling period (in seconds). This is the period with which new conversations are started.

accounts (array):
    Array of account objects. The array must contain at least one value. To be able to use most features of XMPProbe at least two account object are required.
    
    The structure of account objects is defined in the following definition list.

Account objects have the following keys (all keys are mandatory):

jid (string):
    The Jabber ID of the account. Must be a bare Jabber ID, without a resource.

password (string):
    The password for *jid*.

usage (string):
    The usage type for this Jabber account. Must be one of the following: ``answer``, ``ask``, ``debug_periodicbot``, ``debug_timebot``, ``echo``, ``fixme``, ``normal``.
    
    To use the *jid* for monitoring, this value must be set to ``normal``. All other accounts that *jid* should attempt to exchange messages with need to be set to ``normal`` too. At least two accounts with a *usage* of ``normal`` are required for monitoring to work properly.
    
    A value of ``fixme`` indicates that this account is not used for any purpose.
    
    All other possible values are for testing and debugging purposes only. Refer to the documentation of the classes :class:`AskBot` (``ask``), :class:`AnswerBot` (``answer``), :class:`PeriodicBot` (``debug_periodicbot``), :class:`TimeBot` (``debug_timebot``) or :class:`EchoBot` (``echo``), respectively.
