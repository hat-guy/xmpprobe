CSV Log Format
**************

This section outlines the details on the format used for the CSV output of XMPProbe. Each record has an event type. The event type determines how many fields a record has, and what their meaning is.

The following definition list shows what each field is used for, for each event type. Some fields are common between various message formats: ``time`` is the timestamp (floating point number; seconds since Unix time epoch) when the log entry was generated and ``event`` is the name of the event type.

Note that timestamps other than ``time`` may be in a different format.

connect:
    time, jid, event
    
    ``jid`` has connected.

nauth:
    time, jid, event
    
    ``jid`` could not connect, because the authentication failed.

disconnect:
    time, jid, event
    
    ``jid`` has disconnected.

ssubscribe:
    time, jid, event, other_jid
    
    ``jid`` has sent a presence subscription request to ``other_jid``.

rsubscribe:
    time, jid, event, other_jid
    
    ``jid`` has received a presence subscription request from ``other_jid``.

ssubscribed:
    time, jid, event, other_jid
    
    ``jid`` has sent an affirmative answer to a presence subscription request from ``other_jid``.
    Since subscription management happens automatically in parts, this event will never appear in the log file.

rsubscribed:
    time, jid, event, other_jid
    
    ``jid`` has received an affirmative answer from ``other_jid`` to a presence subscription request from ``jid``.

skip:
    time, jid, event, other_jid
    
    ``jid`` took too long handling all that needed to be done in one period. One or more periods were skipped, and as a result ``other_jid`` was not sent an *ask* message.

sask:
    time, jid, event, other_jid, nonce, t_sent
    
    ``jid`` has sent an *ask* message to ``other_jid``. ``nonce`` and ``t_sent`` are the respective fields from the sent message.

sanswer:
    time, jid, event, other_jid, nonce, t_sent
    
    ``jid`` has sent an *answer* message to ``other_jid`` in reply to the *ask* message with the respective fields ``nonce`` and ``t_sent``.

runknown:
    time, jid, event, other_jid, msg
    
    ``jid`` has received the message ``msg`` from ``other_jid`` that does not conform to the specification of *ask* and *answer* messages. This can mean that the message was a corrupt *ask* or *answer* message, or that it was an unrelated message.

rask:
    time, jid, event, other_jid, nonce, t_sent
    
    ``jid`` has received a valid *ask* message from ``other_jid``. ``nonce`` and ``t_sent`` are the respective fields from the received message.
    The path delay can be calculated from ``time`` and ``t_sent``.

ranswer:
    time, jid, event, other_jid, nonce, t_sent, t_rply
    
    ``jid`` has received a valid *answer* message from ``other_jid``. ``nonce`` and ``t_sent`` are the respective fields from the *ask* message that this *answer* message is a reply to, that is the *nonce* field from the *answer* message is decremented, and the ``t_sent`` log entry field is actually *t_rcvd* from the message. ``t_rply`` is *t_sent* from the *answer* message.
    The path delay can be calculated from ``time`` and ``t_rply``.

rsurprise:
    time, jid, event, other_jid, nonce, t_sent, t_rply
    
    ``jid`` has received an *answer* message from ``other_jid``. The message appears to be valid, but it can not be checked whether or not it's an *answer* to an *ask* message that was actually sent. The further log entry fields are defined in the manner of the *ranswer* log entry fields.

Log File Sample
===============

The following short snipped of a CSV log file illustrates what log entries look like for some of the above event types. ::

    1393449830.012724,xmpprobe---0@example.com/longxmpprobe,sask,xmpprobe---0@example.net/longxmpprobe,642973785857,2014-02-26 21:23:50.012054+00:00
    1393449830.163635,xmpprobe---0@example.com/longxmpprobe,ranswer,xmpprobe---0@example.net/longxmpprobe,642973785857,2014-02-26 21:23:50.012054+00:00,2014-02-26 21:23:50.090813+00:00
    1393449840.00329,xmpprobe---0@example.com/longxmpprobe,sask,xmpprobe---0@example.org/longxmpprobe,54361358735,2014-02-26 21:24:00.002678+00:00
    1393449840.046191,xmpprobe---0@example.com/longxmpprobe,runknown,xmpprobe---0@example.org/longxmpprobe,"{""nonce"":54361358735,""comment"":""XMPP monitoring project. Contact mail:xmpprobe@student.ethz.ch for information."",""t_rcvd"":null,""version"":1,""magic"":""xmpP"",""report"":""ping"",""checksum"":1365126023,""t_sent"":""2014-02-26T21:24:00.002678+00:00""}"
    1393449850.011563,xmpprobe---0@example.com/longxmpprobe,sask,xmpprobe---0@jabber.example.com/longxmpprobe,277493021238,2014-02-26 21:24:10.011015+00:00
    1393449860.009387,xmpprobe---0@example.com/longxmpprobe,sask,xmpprobe---0@jabber.example.net/longxmpprobe,1040098151630,2014-02-26 21:24:20.008674+00:00
    1393449860.071305,xmpprobe---0@example.com/longxmpprobe,ranswer,xmpprobe---0@jabber.example.net/longxmpprobe,1040098151630,2014-02-26 21:24:20.008674+00:00,2014-02-26 21:24:20.044593+00:00