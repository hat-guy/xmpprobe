.. XMPProbe documentation master file, created by
   sphinx-quickstart on Wed Feb 12 02:02:19 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

XMPProbe Documentation
**********************

.. toctree::
   :maxdepth: 2
   
   index


.. include:: about.rst


.. include:: xmpprobe-module.rst


.. include:: csvformat.rst


.. include:: xmpprobe-config-syntax.rst


.. include:: analysis.rst


Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
